FROM maven:3.8.6-jdk-11-slim

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN mvn clean package

ENV PORT 8080
EXPOSE $PORT
CMD [ "sh", "-c", "mvn clean -Dserver.port=${PORT} spring-boot:run" ]
